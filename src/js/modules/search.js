export default function () {
  try {
    const search_icon = document.getElementById('icon-search').closest('.icon-search')
    const search_input = document.getElementById('search')
    const search_container = document.querySelector('.search-container')
    const cover = document.getElementById('cover-ctn-search')
    const box_search = document.getElementById('box-search')

    search_icon.addEventListener('click', event => {
      search_container.classList.add('is-active-search')
      cover.style.display = "block";
    })

    function clearSearch() {
      search_input.value = ""
    }

    function debounce(callback, time) {
      let timeoutId
      return function () {
        if (timeoutId)
          clearTimeout(timeoutId)
        const context = this
        const args = arguments
        timeoutId = setTimeout(() => {
          callback.apply(context, args)
        }, time)
      }
    }
    function internal_search_engine(filter) {
      if(!filter.length) {
        box_search.classList.remove('is-block')
        return
      }
      const arrLi = [...box_search.querySelectorAll('li')]
      arrLi.forEach(value=>{
        value.classList.remove('is-block')
        const textValue = value.children[0].textContent.toUpperCase()
        if (textValue.includes(filter)) {
          console.log('entre');
          value.classList.add('is-block')
          box_search.classList.add('is-block')
        }else{
          value.classList.remove('is-block')
        }
      })
    }
    const renderResultDebounce = debounce(internal_search_engine,500)

    cover.addEventListener('click', event => {
      cover.style.display = "none";
      search_container.classList.remove('is-active-search')
      clearSearch()
      box_search.classList.remove('is-block')
    })


    search_input.addEventListener('keyup', ()=>{
      renderResultDebounce(search_input.value.trim().toUpperCase())
    })


    window.onkeydown = function (event) {
      const key = event.keyCode
      if (key === 27) {
        cover.style.display = "none";
        search_container.classList.remove('is-active-search')
        clearSearch()
        box_search.classList.remove('is-block')
      }
    }

  } catch {}
}
