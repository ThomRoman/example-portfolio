export default function () {
  const iconMenu = document.getElementById("icon-menu");
  const mainMenu = document.getElementById("main-menu__nav");
  const main = document.getElementById('main')
  const main_footer = document.querySelector('.main-footer')

  if(!iconMenu || !mainMenu || !main || !main_footer)
    return;

  function toggleMenu() {
    mainMenu.classList.toggle("is-open");
    main.classList.toggle('is-active')
    main_footer.classList.toggle('is-active')
  }
  function removeMenu() {
    mainMenu.classList.remove("is-open");
    main.classList.remove('is-active')
    main_footer.classList.remove('is-active')
  }

  const mediumBp = matchMedia("(min-width:768px)");
  function closeMenu(mediaQueryList) {
    mediaQueryList.matches ? removeMenu() : null;
  }

  closeMenu(mediumBp);
  mediumBp.addListener(closeMenu);
  iconMenu.addEventListener("click", toggleMenu);
}
