export default function(){
  const buttonUp = document.getElementById("button-up");
  if(!buttonUp) return
  window.onscroll = function(){
    const scroll = document.documentElement.scrollTop;
    if(scroll > 500){
      buttonUp.classList.add('show');
    }else if(scroll < 500){
      buttonUp.classList.remove('show');
    }
  }
  function scrollUp(){
    const currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
    if (currentScroll > 0){
      window.requestAnimationFrame(scrollUp);
      window.scrollTo (0, currentScroll - (currentScroll / 5));
    }
  }
  const elementButton = buttonUp.closest('.button-up')
  elementButton.addEventListener('click',scrollUp)

}
